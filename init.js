const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./token/config");
const embed = new Discord.RichEmbed();



// client.registry.registerGroup('simple', 'Simple');
// client.registry.registerGroup('music', 'Music');
// client.registry.registerDefaults();
// client.registry.registerCommandsIn(__dirname + '/commands');

client.on("ready", () => {
    console.log(`Bot foi iniciado, com ${client.users.size} usuários, em ${client.channels.size} canais, em ${client.guilds.size} servidores.`);
    client.user.setActivity('Digite >help - Irei te ajudar');
    // client.user.setActivity(`Eu estou em ${client.guilds.size} servidores`);
    client.user.setPresence({ game: { name: 'Digite >help - Irei te ajudar', type: 1, url: 'https://www.twitch.tv/dimitribellion' } });
    // caso queira o bot trasmitindo use:
    /*
        client.user.setPresence({ game: { name: 'comando', type: 1, url: 'https://www.twitch.tv/ladonegro'} });
        //0 = Jogando
        //  1 = Transmitindo
        //  2 = Ouvindo
        //  3 = Assistindo
          */
});

// client.registry
//     .registerDefaultTypes()
//     .registerGroups([
//         ['simple', 'music']
//     ])
//     .registerDefaultGroups()
//     .registerDefaultCommands()
//     .registerCommandsIn(path.join(__dirname, 'commands'));

client.on("guildCreate", guild => {
    console.log(`O bot entrou nos servidor: ${guild.name} (id: ${guild.id}). População: ${guild.memberCount} membros!`);
    client.user.setActivity(`Estou em ${client.guilds.size} servidores`);
});

client.on("guildDelete", guild => {
    console.log(`O bot foi removido do servidor: ${guild.name} (id: ${guild.id})`);
    client.user.setActivity(`Estou em ${client.guilds.size} servidores`);
});


client.on("message", async message => {

    if (message.author.bot) return;
    if (message.channel.type === "dm") return;
    if (!message.content.startsWith(config.prefix)) return;

    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const comando = args.shift().toLowerCase();

    //comando dono
    if (comando === "dono") {
        await message.channel.send("O meu dono é <@213004769814380545>");
    }

    // coamdno ping
    if (comando === "ping") {
        message.delete().catch(xisde => { });
        const m = await message.channel.send("Ping?");
        m.edit(`A Latência é ${m.createdTimestamp - message.createdTimestamp}ms. A Latencia da API é ${Math.round(client.ping)}ms`);
    }
    //comando falar
    if (comando === "say") {
        const sayMessage = args.join(" ");
        message.delete().catch(O_o => { });
        message.channel.send(sayMessage);
    }
    //comando apagar
    if (comando === "apagar") {
        if (!message.member.roles.some(r => [".IA"].includes(r.name))) {
            sayMessage = args.join(" ");
            deleteCount = parseInt(args[0], 10);
            message.delete().catch(O_o => { });
            if (!deleteCount || deleteCount < 2 || deleteCount > 100)
                return message.reply("Forneça um número entre 2 e 100 para o número de mensagens a serem excluídas.");

            const fetched = await message.channel.fetchMessages({ limit: deleteCount });
            message.channel.bulkDelete(fetched)
                .catch(error =>
                    message.reply(`Não foi possível deletar mensagens devido a: ${error}`));
        }
    }
    // comando chutar 
    if (comando === "kick") {
        //adicione o nome dos cargos que vc quer que use esse comando!
        if (!message.member.roles.some(r => ["</Json>", ".IA"].includes(r.name)))
            return message.reply("Desculpe, você não tem permissão para usar isto!");
        let member = message.mentions.members.first() || message.guild.members.get(args[0]);
        if (!member)
            return message.reply("Por favor mencione um membro válido deste servidor");
        if (!member.kickable)
            return message.reply("Eu não posso expulsar este usuário! Eles pode ter um cargo mais alto ou eu não tenho permissões de expulsar?");

        let reason = args.slice(1).join(' ');
        if (!reason) reason = "Nenhuma razão fornecida";

        await member.kick(reason)
            .catch(error => message.reply(`Desculpe ${message.author} não consegui expulsar o membro devido o: ${error}`));
        message.reply(`${member.user.tag} foi kickado por ${message.author.tag} Motivo: ${reason}`);

    }
    // comando ban
    if (comando === "ban") {
        //adicione o nome do cargo que vc quer que use esse comando!
        if (!message.member.roles.some(r => ["</Json>", ".IA"].includes(r.name)))
            return message.reply("Desculpe, você não tem permissão para usar isto!");
        let member = message.mentions.members.first();
        if (!member)
            return message.reply("Por favor mencione um membro válido deste servidor");
        if (!member.bannable)
            return message.reply("Eu não posso banir este usuário! Eles pode ter um cargo mais alto ou eu não tenho permissões de banir?");
        let reason = args.slice(1).join(' ');
        if (!reason) reason = "Nenhuma razão fornecida";
        await member.ban(reason)
            .catch(error => message.reply(`Desculpe ${message.author} não consegui banir o membro devido o : ${error}`));
        embed
            .setTitle("Usuário:")
        message.reply(`${member.user.tag} foi banido por ${message.author.tag} Motivo: ${reason}`);
    }

    if (comando === "status") {
        const st = await message.channel.send("On?");
        st.edit(`Estou cuidando de ${client.users.size} usuários, em ${client.channels.size} canais, em ${client.guilds.size} servidores.`);
    }

    if (comando === "help") {
        await message.channel.send('**Meus comandos são** : `prefix`(>) _livraria_, _ping_, _say_, _apagar_, _kick_, _ban_, _status_ e _help_.');
    }

    if (comando === "criador") {
        const amor = "<@251850129542217728>";
        const embed = {

            "author": {
                "name": message.author.tag,
                "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png"
            },
            "image": {
                "url": "https://cdn.discordapp.com/avatars/213004769814380545/a_4f0426d77c6b4d30f3d77713c010c794.gif"
            },
        }
        message.channel.send({ embed })
        // .setTitle("Criador")
        // .setAuthor(message.author.tag, message.author.avatarURL)
        // /*
        //  * Alternatively, use "#00AE86", [0, 174, 134] or an integer number.
        //  */
        // .setColor(000000)
        // .setDescription("Dimitri Bellion")
        // .setFooter("Programmer", message.author.avatarURL)
        // .setImage(message.author.avatarURL)
        // .setThumbnail("https://i.redd.it/a8keeuutawx01.gif")
        // /*
        //  * Takes a Date object, defaults to current date.
        //  */
        // .setTimestamp()
        // .setURL("https://discord.js.org/#/docs/main/indev/class/RichEmbed")
        // .addField("**Idade:**",
        //     "_15 anos_")
        // /*
        //  * Inline fields may not display as inline if the thumbnail and/or image is too big.
        //  */
        // .addField("**Desenvolvedor Junior**", "_HMTL5, CSS, JS, Java, C++, ANGULAR, NODE, NPM, GIT._", true)
        // /*
        //  * Blank field, useful to create some space.
        //  */
        // // .addBlankField(true)
        // .addField("**Namorado da senhorita**", amor)

    }

    if (comando === "embed") {
        embed
            .setImage("https://cdn.discordapp.com/attachments/464546351548596229/513030219133747200/image1.jpg")
            message.channel.send({ embed })

    }

    if (comando === "meme") {
        memeRandom = "https://cdn.discordapp.com/attachments/464546351548596229/513030219133747200/image1.jpg"
        randomNumber = Math.floor(Math.random() * (3 - 1) + 1);
        if (randomNumber == 2) {

        }
        // message.reply("Se fodeu, levou rola `_(_`");
        message.channel.send({ embed })
    }

    if (comando === "livraria") {
        message.channel.send("```Livros em PDF:\n" +
                                "https://drive.google.com/open?id=1H5C6XKVE_Pu5vgtCMHPpQFTVEu0d9PZ2\n" +
                                "https://goalkicker.com/```\n" +
                                "```Video Aulas Free:\n" +
                                "https://onebitcode.com/```\n" +
                                "```Sites para estudar:\nhttps://www.freecodecamp.org/\n" +
                                "https://www.codecademy.com/pt-BR```")
    }

    if (comando == "fonte") {
        if (!message.member.roles.some(r => ["♠", ".IA"].includes(r.name)))
            return message.reply("Desculpe, você não tem permissão para usar isto!");

    }

});


client.login(config.token);